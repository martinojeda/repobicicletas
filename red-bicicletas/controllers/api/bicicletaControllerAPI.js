var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function(req,res){
    console.log(req.body);
    var lat_n = Number(req.body.lat);
    var lng_n = Number(req.body.lng);
    var doc = { "code": req.body.code, 
                "color": req.body.color, 
                "modelo": req.body.modelo, 
                "ubicacion": [lat_n, lng_n]
            };
    console.log(doc);
    var bici = new Bicicleta(doc);
    //var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo, [lat_n, lng_n]);
    Bicicleta.add(bici);
    //console.log(bici);
    res.status(200).json({
        bicicleta: bici,
        Description: "Bici " + bici.code + " Created"
    })
}

exports.bicicleta_update = function(req, res){
    console.log(req.params.id);
    var lat_n = Number(req.body.lat);
    var lng_n = Number(req.body.lng);
    var id = req.params.id;
    var newbici = Bicicleta.findById(id);
    console.log(newbici.code);
    
    newbici.code = req.body.code;
    newbici.color = req.body.color;
    newbici.modelo = req.body.modelo;
    newbici.ubicacion = [lat_n, lng_n];
    
    res.status(200).json({"mensaje": "se ha actualizado la bici"
    });
  }

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}