var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
var mailer = require('../mailer/mailer');
var Token = require('./token');

var bcrypt = require('bcrypt');
var crypto = require('crypto');
var saltRounds = 10;


var validateEmail = function (email) {
    const regularExpression = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    return regularExpression.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type:String,
        trim:true,
        required:[true,'El nombre es obligatorio']
    },
    email:{
        type:String,
        trim:true,
        required:[true,'El email es obligatorio'],
        lowercase: true,
        unique:true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/] //valdate in part of moongo
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
      },
      passwordResetToken: String,
      passwordResetTokenExpires: Date,
      verificado: {
        type: Boolean,
        default: false
      }
    });

usuarioSchema.pre('save', function(next){
    if ( this.isModified('password') ){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next(); 
    });
    
usuarioSchema.methods.validPassword = function(password) {
  console.log("compara pass: " + bcrypt.compareSync(password, this.password))
  return bcrypt.compareSync(password, this.password);
}


usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreateByGoogle(condition, callback) {

  const self = this;
  console.log(condition);
  self.findOne({
      $or: [
          { 'googleId': condition.id }, { 'email': condition.emails[0].value }
      ]
  }, (err, result) => {
      if (result) {
          callback(err, result)
      } else {
          console.log('--------- CONDITION ---------');
          console.log(condition);
          let values = {};
          values.googleId = condition.id;
          values.email = condition.emails[0].value;
          values.nombre = condition.displayName || 'SIN NOMBRE';
          values.verificado = true;
          values.password = 'oauth';
          console.log('----------- VALUES ----------');
          console.log(values);
          self.create(values, (err, result) => {
              if (err) console.log(err);
              return callback(err, result)
          })
      }
  })
};


usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreateByFacebook(condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne({
      $or: [
          { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
      ]
  }, (err, result) => {
      if (result) {
          callback(err, result)
      } else {
          console.log('--------- CONDITION ---------');
          console.log(condition);
          let values = {};
          values.facebookId = condition.id;
          values.email = condition.emails[0].value;
          values.nombre = condition.displayName || 'SIN NOMBRE';
          values.verificado = true;
          values.password = crypto.randomBytes(16).toString('hex');
          console.log('----------- VALUES ----------');
          console.log(values);
          self.create(values, (err, result) => {
              if (err) console.log(err);
              return callback(err, result)
          })
      }
  })
};



usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb){
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId, 
        desde: desde, 
        hasta: hasta
    });
    
    reserva.save(cb);
}
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario '});

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email;
    token.save((err) => {
      if ( err ) { return console.log(err.message)}
      const mailOptions = {
        from: 'no-reply@redbicicletas.com',
        to: email_destination,
        subject: 'Verificacion de cuenta',
        text: 'Hola,\n\n' 
        + 'Por favor, para verificar su cuenta haga click en este link: \n' 
        + 'http://localhost:3000'
        + '\/token/confirmation\/' + token.token + '\n'
      }

      console.log('Se ha enviado un email de bienvenida a: ' + email_destination)
      mailer.sendMail(mailOptions, function(err){
      if( err ) { return console.log(err.message) } 
        console.log('Se ha enviado un email de bienvenida a: ' + email_destination)
      })
    })
   }

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err){
        if (err) {return cb(err);}

        const mailOptions ={
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de passord de cuenta',
            text:  'Hola, por favor para resetear password de su cuenta haga click en: \n' + 
            'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err){
            if (err) {return cb(err);}

            console.log('Se envio un email para resetear el password a: ' + email_destination +'.');
        });

        cb(null);
    });
};
 
module.exports = mongoose.model('Usuario', usuarioSchema);