require("dotenv").config();
require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session')
var GoogleStrategy = require('passport-google-oauth').OAuthStrategy;
var MongoDBStore = require('connect-mongodb-session')(session);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var biciRouter = require('./routes/bicicletas');
var biciRouterAPI = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');
var tokenRouter = require('./routes/token');
var usuariosRouter = require('./routes/usuarios');
var Token = require('./models/token');
var Usuario = require('./models/usuario');
const jwt = require('jsonwebtoken');
var app = express();
/*
var mongoose = require("mongoose");
var mongoDB = process.env.MONGO_URI;
*/

var mongoose = require('mongoose'); 
//var mongoDB = 'mongodb://localhost/test';
var mongoDB = process.env.MONGO_URI;


/*
let store
store = new session.MemoryStore;
*/
let store

if (process.env.NODE_ENV === "dev"){
  store = new session.MemoryStore;

}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions"
  });
  store.on("error", function(error){
    assert.ifError(error)
    assert.ok(false)
  }) 
}

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000}, 
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicicletas' 
}))




//console.log(mongoDB);

mongoose.connect(mongoDB, {
  useNewUrlParser: true,  
  useCreateIndex: true,
  useUnifiedTopology: true
})
mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.on('error',console.error.bind(console,"error connection DB : "))
db.once('open', ()=>{
  console.log("conected")
}) 

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('secretKey', 'red_bicis');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize())
app.use(passport.session());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn,biciRouter);
app.use('/api/auth', authAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);
app.use('/api/bicicletas/', validarUsuario,biciRouterAPI);
app.use('/usuarios', loggedIn, usuariosRouter);
app.use('/token', tokenRouter);

//Handle Routes from app.js
app.get('/login', (req, res)=>{
  res.render('session/login')
})

//Handle Routes from app.js
app.get('/policy', (req, res)=>{
  res.sendfile('public/policy.html')
})

app.use('/googlebde0ae1ca71b514c', (req, res)=>{
  res.sendfile('public/googlebde0ae1ca71b514c.html')
})

app.get('/auth/google', 
  passport.authenticate("google", {scope: [
    "https://www.googleapis.com/auth/plus.login",
    "https://www.googleapis.com/auth/plus.profile.emails.read"
  ]}))


app.get('/auth/google/callback', 
  passport.authenticate("google", {
      successRedirect: "/",
      failureRedirect: "/error"
  })
)


passport.use(new GoogleStrategy({
  consumerKey: process.env.GOOGLE_CLIENT_ID,
  consumerSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.HOST + "/auth/google/callback"
},
function(token, tokenSecret, profile, cb) {
    Usuario.findOneOrCreateByGoogle( profile , function (err, user) {
      return cb(err, user);
    });
}
));

app.post('/login', (req, res, next)=> {
  passport.authenticate('local', (err, usuario, info)=>{
    console.log(req.body);
    // console.log("err " + err);
    // console.log("usr " + usuario);
    // console.log("info " + info.message);
    if(err) return next(err);
    
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, err =>{
      if(err) return next(err);
      return res.redirect('/');
    });
  }) (req, res, next);
})

app.get('/logout', (req, res)=>{
  req.logOut() 
  res.redirect('/')
})


app.get('/forgotPassword', (req, res)=>{
  res.render('session/forgotPassword')
})

app.post('/forgotPassword', (req, res, next)=>{
  Usuario.findOne({email: req.body.email}, (err, usuario)=>{
    if(!usuario) return res.render('session/forgotPassword', {info: {message: 'No existe la clave'}});
    
    usuario.resetPassword(err=>{
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    })
    res.render('session/forgotPasswordMessage')
  })
})

app.get('/resetPassword/:token', (req, res, next)=>{
  Token.findOne({token: req.params.token}, (err, token)=>{
    if(!token) return res.status(400).send({type: 'not-verified', msg: 'No existe clave'})

    Usuario.findById(token._userId, (err, usuario)=>{
      if(!usuario) return res.status(400).send({msg: 'No existe usuario'});
      res.render('session/resetPassword', {errors: {}, usuario: usuario})
    })
  })
})

app.post('/resetPassword', (req, res)=>{
  if(req.body.password != req.body.confirm_password) {
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coinciden las contraseñas'}}});
    return;
  }
  Usuario.findOne({email: req.body.email}, (err, usuario)=>{
    usuario.password = req.body.password;
    usuario.save(err=>{
      if(err){
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario});
      } else {
        res.redirect('/login')
      }
    })
  })
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


function loggedIn(req, res, next){
  if(req.user){
      next();
  } else {
      console.log('usuario sin logearse');
      res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded){
      if(err) {
          res.json({status:"error", message:err.message, data:null});
      }else{
          req.body.userId = decoded.id;
          console.log('jwt verify ' + decoded);
          next();
      }
  })
}


module.exports = app;
