var mongoose = require('mongoose');
var Bicicletadb = require('../../models/bicicleta');

describe("testing Bicicletas", () =>{
    beforeEach(function(done){
        setTimeout(function() {
            var mongoDB = 'mongodb://localhost/test'
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true})
            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection DB error'));
            db.once('open', function(){
            console.log('Conectado!');
            });
            done();
          }, 100);
    });

    afterEach(function(done){
        Bicicletadb.deleteMany({}, function(err,success){
            if(err) console.log(err);
            done()
        })
        //mongoose.disconnect();
    })

    /*
    describe("Crear bicicleta", () =>{
        it("agregar bici", ()=>{
            var bici = Bicicletadb.createInstance(1, "rojo", "mountain", [-34.6011, -58.381])
            console.log(bici);

            expect(bici.code).toBe(1);
        })
    })
*/
    describe('Bicicleta.add', () => {
        it('Agregar bici', (done) => {
          var aBici = new Bicicletadb({code: 1, color:"verde", modelo:"urbana"});
          Bicicletadb.add(aBici, function(err, newBici){
            if ( err ) console.log(err);
            Bicicletadb.allBicis(function(err, bicis){
              console.log(bicis[0].code)
              expect(bicis[0].code).toEqual(aBici.code);
              done(); 
            });
          });
        });    
      });
    
    describe("allbicis", () =>{
        it("all bici", (done)=>{
            Bicicletadb.allBicis(function (err,bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        })
    })

    describe('Bicicleta.findByCode', () => {
        it('Buscar bicicleta code = 1', (done) => {
            Bicicletadb.allBicis(function(err, bicis){
            expect(bicis.length).toBe(0); // We use expect here becaouse in each test  we delete the data from test database so data has to be empty
    
            //we add a bicicleta 
            var aBici = new Bicicletadb({code: 1, color: "verde", modelo:"urbana"});
            Bicicletadb.add(aBici, function(err, newBici){
              if (err) console.log(err);
            
              //we add a second bicicleta 
              var aBici2 = new Bicicletadb({code:2, color: "roja", modelo:"deportiva"});
              Bicicletadb.add(aBici2, function(err, newBici){
                if (err) console.log(err);

                //test if we add correectly the first bicicleta added
                Bicicletadb.findByCode(1, function(error, targetBici){
                  expect(targetBici.code).toBe(aBici.code);
                  expect(targetBici.color).toBe(aBici.color);
                  expect(targetBici.modelo).toBe(aBici.modelo);
                  done();
                });
              });
            });
          });
          
        });    
      });



})

/*
describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, { useNewUrlParser: true}); 

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() { 
            console.log('estamos conectados!');
            done();
        }); 
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);     
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicletas.allBicis', () => {
        it('comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });        
        });
    });

    describe('Bicicletas.add', () => {
        it('Agregamos una', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
    
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
    
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
    
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
    
                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
    
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);
        
                    var aBici2 = new Bicicleta({code: 1, color: "roja", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
    
                            done();
                        });
                    });
                });
            });
        });
    });
});
*/

