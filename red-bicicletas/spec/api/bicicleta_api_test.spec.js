var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Testing API Bicicletas', () => {
    beforeEach(function(done){
        setTimeout(function() {
            var mongoDB = 'mongodb://localhost/test'
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true})
            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection DB error'));
            db.once('open', function(){
            console.log('Conectado!');
            });
            done();
          }, 100);
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err);
            done()
        })
        //mongoose.disconnect();
    })

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                //console.log(body);
                //expect(response.statusCode).toBe(200);
                //expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
        var headers = {'content-type' : 'application/json'};
        var aBici = '{ "code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
        request.post({
            headers: headers,
            url: base_url + '/create',
            body: aBici,
            },
            function(error, response, body) {
            expect(response.statusCode).toBe(200);
            done();
        });
        });
    });
});


/*
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
const { ConsoleReporter } = require('jasmine');
//var server = require('../../bin/www');



var base_url = 'http://localhost:3000/api/bicicletas';

describe('Testing API Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, { useNewUrlParser: true}); 

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:')); 
        db.once('open', function() { 
            console.log('estamos conectados!');
            done();
        }); 
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                //console.log(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "ubicacion": [-34, -54] }';
            
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
                }, function(error, response, body) {               
                    expect(response.statusCode).toBe(200);

                    var bici = JSON.parse(body).bicicleta;
                    console.log(bici);
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-54);
                    done();
            });
        });
    });
});



*/