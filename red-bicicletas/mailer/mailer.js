const nodemailer = require("nodemailer")
const sgTransport = require("nodemailer-sendgrid-transport")

let mailConfig

if(process.env.NODE_ENV === "production"){
  const options = {
    auth: {
      api_key: process.env.SENDGRID_APIKEY 
    }
  }
  mailConfig = sgTransport(options)
}else{
  if(process.env.NODE_ENV === "stagging"){
    console.log("stagging")
    const options = {
      auth: {
        api_key: process.env.SENDGRID_APIKEY 
      }
    }
    mailConfig = sgTransport(options)

  } else {
    mailConfig = ({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
          user: process.env.LOCAL_MAIL,
          pass: process.env.LOCAL_PASS
      }
    });
  }
}

module.exports = nodemailer.createTransport(mailConfig)